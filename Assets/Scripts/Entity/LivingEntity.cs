using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivingEntity : MonoBehaviour
{
    public string food;

    public float searchRadius = 5f;
    public float mutationChance = 0.1f;

    public float hunger = 0.5f;
    public float hungerRate = 0.5f;
    private float hungerTimer;

    private float birthCooldown = 60f;
    private float birthTimer;
    private bool hasBirth;

    public float jumpCooldown = 1f;
    public float jumpForce = 2f;
    private float jumpTimer;

    private float rotationSpeed = 10f;

    private bool isMoving = false;
    private Vector3 wander;

    private GameObject nextFood;
    private Collider[] colliders;
    private Rigidbody rb;

    void RandomWander()
    {
        wander = transform.position + new Vector3(Random.Range(-4f, 4f), 0f, Random.Range(-4f, 4f));
    }

    // Start is called before the first frame update
    void Start()
    {
        RandomWander();
        Mutate();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
       
        Search();

        hungerTimer -= Time.deltaTime;
        birthTimer -= Time.deltaTime;
        if (hungerTimer <= 0)
        {
            hungerTimer = (1 / hungerRate) + 5;
            hunger += hungerRate / 10;
        }

        if (hunger >= 1)
        {
            Destroy(gameObject);
        }

        if (birthTimer <= 0)
        {
            hasBirth = false;
        }

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, searchRadius);
    }

    void Search()
    {
        colliders = Physics.OverlapSphere(transform.position, searchRadius);
        nextFood = GetNearestFood(colliders);
        if (nextFood)
        {
            isMoving = true;
            Move(nextFood.transform.position);
        }

        else isMoving = false;

        if (!isMoving && !nextFood)
        {
            Move(wander);
            isMoving = true;
        }
    }

    void Move(Vector3 target)
    {
        //transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        Vector3 move = target - transform.position;

        jumpTimer -= Time.deltaTime;
        if (jumpTimer <= 0)
        {
            jumpTimer = jumpCooldown;
            //rb.MovePosition(target);
            rb.AddForce((move.normalized) * jumpForce + Vector3.up * 2 + (Vector3.up * jumpForce / 10), ForceMode.Impulse);
        }
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(target), Time.deltaTime * rotationSpeed);

        if (Vector3.Distance(transform.position, target) < 0.5f)
        {
            RandomWander();
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.tag == food)
        {
            hunger -= 0.05f;
            if (transform.localScale.x < 0.25f)
            {
                transform.localScale += new Vector3(0.01f, 0.01f, 0.01f);
            }

            if (hunger < 0f && !hasBirth)
            {
                hunger = 0.5f;
                GameObject inst = Instantiate(gameObject, transform.position + new Vector3(1, 0, 0), Quaternion.identity);
                inst.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);
                inst.transform.SetParent(transform.parent);
                inst.name = name;

                LivingEntity livingEntity = inst.GetComponent<LivingEntity>();
                foreach (var property in livingEntity.GetType().GetFields())
                {
                    try
                    {
                        property.SetValue(this, float.Parse(property.GetValue(this).ToString()));
                    }
                    catch { }
                }
                livingEntity.hunger = 0.5f;

                hasBirth = true;
                birthTimer = birthCooldown;
            }
            isMoving = false;
            Destroy(other.gameObject);
        }
    }

    void Mutate()
    {
        bool isMutated = false;
        var converter = new Dictionary<string, float>
        {
            { "mutationChance", Random.Range(-0.1f, 0.1f) },
            { "hungerRate", Random.Range(-0.1f, 0.1f) },
            { "searchRadius", Random.Range(-1f, 1f) },
            { "jumpCooldown", Random.Range(-0.05f, 0.05f) },
            { "jumpForce", Random.Range(-0.2f, 0.2f) }
        };
        var mutated = new Dictionary<string, float> { };
        foreach (KeyValuePair<string, float> entry in converter)
        {
            if (Random.Range(0f, 1f) < mutationChance) {
                mutated.Add(entry.Key, entry.Value);
                isMutated = true;
            }
        }

        foreach (var property in GetType().GetFields())
        {
            try
            {
                property.SetValue(this, float.Parse(property.GetValue(this).ToString()) + mutated?[property.ToString().Split(' ')[1]]);
                if (float.Parse(property.GetValue(this).ToString()) < 0.05f)
                    property.SetValue(this, 0.05f);
            }
            catch { }
        }

        if (isMutated)
            GetComponent<Renderer>().material.color = Random.ColorHSV();

        if (jumpCooldown < 0.5f) jumpCooldown = 0.5f;
    }

    GameObject GetNearestFood(Collider[] colliders)
    {
        GameObject nearest = null;
        float minDist = Mathf.Infinity;
        foreach (var obj in colliders)
        {
            float dist = Vector3.Distance(obj.transform.position, transform.position);
            if (dist < minDist)
            {
                if (obj.tag == food)
                {
                    nearest = obj.gameObject;
                    minDist = dist;
                }
            }
        }
        return nearest;
    }

    

}
