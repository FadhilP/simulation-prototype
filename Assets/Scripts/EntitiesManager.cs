using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EntitiesManager : MonoBehaviour
{
    private float averageSearchRadius;
    private float averageJumpCooldown;
    private float averageJumpForce;
    private float averageHungerRate;

    public Text details;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        foreach(Transform child in transform)
        {
            var comp = child.GetComponent<LivingEntity>();
            averageHungerRate += comp.hungerRate;
            averageJumpCooldown += comp.jumpCooldown;
            averageJumpForce += comp.jumpForce;
            averageSearchRadius += comp.searchRadius;
        }
        averageHungerRate /= transform.childCount + 1;
        averageJumpCooldown /= transform.childCount + 1;
        averageJumpForce /= transform.childCount + 1;
        averageSearchRadius /= transform.childCount + 1;

        details.text = string.Format("Details:\nSearch Radius: {0}\nJump Cooldown: {1}\nJump Force: {2}\nHunger Rate: {3}",
            averageSearchRadius, averageJumpCooldown, averageJumpForce, averageHungerRate);
    }
}
