using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public float cooldown = 2f;
    private float timer = 0f;
    public float count = 2f;
    public float radius = 30f;
    public float limit = 1000f;
    public GameObject entity;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            for (int i = 0; i < count; i++)
            {
                if (GameObject.FindGameObjectsWithTag(entity.tag).Length < limit)
                {
                    GameObject inst = Instantiate(entity, new Vector3(Random.Range(-radius, radius), 0.5f, Random.Range(-radius, radius)), Quaternion.identity);
                    inst.transform.SetParent(transform);
                    timer = cooldown;
                }
            }
           
        }
    }
}
